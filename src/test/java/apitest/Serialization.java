package apitest;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.File;
import java.io.IOException;

public class Serialization {

    public static void main(String[] args) throws IOException {
        ObjectMapper ob = new ObjectMapper();
       // CarObject c = new CarObject("black","single");
CarObject c = new CarObject();
c.setColor("white");
c.setName("d");
c.setType("df");



        ob.writeValue(new File(System.getProperty("user.dir")+"/car.json"), c);

        String carJson = ob.writerWithDefaultPrettyPrinter().writeValueAsString(c);
        System.out.println("car json as string is :"+carJson);

    }
}
