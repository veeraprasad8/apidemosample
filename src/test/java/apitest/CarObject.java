package apitest;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

public class CarObject {
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String name;
    private String color;
    @JsonIgnore
    private String type;
    //@JsonProperty("strVal")

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }



    public CarObject(String color, String type){

        this.color=color;
        this.type=type;
    }


    CarObject(){}
}
