package apitest;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.restassured.RestAssured;
import io.restassured.common.mapper.TypeRef;
import io.restassured.parsing.Parser;
import io.restassured.response.Response;

import java.util.Arrays;
import java.util.List;

public class ArrayJsonConvert {



    public static void main(String[] args) throws Exception {
        RestAssured.defaultParser = Parser.JSON;
        Response r =  RestAssured.given().get("https://jsonplaceholder.typicode.com/posts");
        r.prettyPrint();
      //  User[] userarray = r.as(User[].class);
       ObjectMapper objectMapper = new ObjectMapper();


        List<User> cars1 = objectMapper.readValue(r.asString(), new TypeReference<Object>(){});

        System.out.println(cars1.size());
        System.out.println(cars1.get(0).getUserId());
        //List<Car> cars1 = objectMapper.readValue(jsonArray, new TypeReference<List<Car>>(){});
      /*  System.out.println(userarray.length);

        for(User u : userarray) {

            if(u.getId()==100) {
                System.out.println("userid " + u.getUserId());
                System.out.println("id " + u.getId());
                System.out.println("title " + u.getTitle());
                System.out.println("body " + u.getBody());*/

    }




}

class User{


    private int UserId;
    private int id;
    private String title;
    private String body;

    public int getUserId() {
        return UserId;
    }

    public void setUserId(int userId) {
        UserId = userId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }
}